<?php


/**
 * This is a stub file of tarantool-php extension for various PHP IDEs.
 * @see https://github.com/tarantool/tarantool-php
 */
class Tarantool
{
    public const ITERATOR_EQ = 'eq';
    public const ITERATOR_REQ = 'req';
    public const ITERATOR_ALL = 'all';
    public const ITERATOR_LT = 'lt';
    public const ITERATOR_LE = 'le';
    public const ITERATOR_GE = 'ge';
    public const ITERATOR_GT = 'gt';
    public const ITERATOR_BITS_ALL_SET = 'bits_all_set';
    public const ITERATOR_BITS_ANY_SET = 'bits_any_set';
    public const ITERATOR_BITS_ALL_NOT_SET = 'bits_all_not_set';
    public const ITERATOR_OVERLAPS = 'overlaps';
    public const ITERATOR_NEIGHBOR = 'neighbor';

    /**
     * Creates a Tarantool client
     *
     * @see https://github.com/tarantool/tarantool-php#tarantool__construct
     * @param string $host [optional, default='localhost']
     * @param int $port [optional, default=3301]
     * @param string $user [optional, default='guest']
     * @param string|null $password
     * @param string|null $persistent_id [optional] Set it, and connection will be persistent, if persistent in config isn't set
     */
    public function __construct(
        string $host = 'localhost',
        int $port = 3301,
        string $user = 'guest',
        string $password = null,
        string $persistent_id = null
    ) {
    }

    /**
     * Explicit connect to Tarantool Server. If not used, then connection will be opened on demand.
     *
     * @see https://github.com/tarantool/tarantool-php#connection-manipulations
     * @throws Exception
     */
    public function connect(): bool
    {
    }

    /**
     * Explicitly close connection to Tarantool Server. If you're using persistent connections, then it'll be saved to connection pool.
     *
     * @see https://github.com/tarantool/tarantool-php#tarantooldisconnect
     */
    public function disconnect(): bool
    {
    }

    /**
     * Remove space/index schema from client.
     *
     * @see https://github.com/tarantool/tarantool-php#tarantoolflushschema
     */
    public function flushSchema(): bool
    {
    }

    /**
     * Ping Tarantool server.
     *
     * @see https://github.com/tarantool/tarantool-php#tarantoolping
     * @throws Exception
     */
    public function ping(): bool
    {
    }

    /**
     * Execute select query from Tarantool server.
     *
     * @see https://github.com/tarantool/tarantool-php#tarantoolselect
     * @param string|int $space
     * @param string|int|array $key [optional, default=[]]
     * @param string|int $index [optional, default=0]
     * @param int $limit [optional, default=PHP_INT_MAX]
     * @param int $offset [optional, default=0]
     * @param string|int $iterator [optional, default='req']
     * @return array
     * @throws Exception
     */
    public function select(
        $space,
        $key = [],
        $index = 0,
        int $limit = PHP_INT_MAX,
        int $offset = 0,
        $iterator = self::ITERATOR_REQ
    ): array {
    }

    /**
     * Insert (if not exists query with same PK) or Replace tuple.
     *
     * @see https://github.com/tarantool/tarantool-php#tarantoolinsert-tarantoolreplace
     * @param string|int $space
     * @param array $tuple
     * @return array
     * @throws Exception
     */
    public function insert($space, array $tuple): array
    {
    }

    /**
     * Insert (if not exists query with same PK) or Replace tuple.
     *
     * @see https://github.com/tarantool/tarantool-php#tarantoolinsert-tarantoolreplace
     * @param string|int $space
     * @param array $tuple
     * @return array
     * @throws Exception
     */
    public function replace($space, array $tuple): array
    {
    }


    /**
     * Call stored procedure
     *
     * @see https://github.com/tarantool/tarantool-php#tarantoolcall
     * @param string $procedure
     * @param mixed|null $args [optional]
     * @param array $opts [optional, default=[]]
     * @return mixed
     * @throws Exception
     */
    public function call(string $procedure, $args = null, array $opts = [])
    {
    }

    /**
     * Evaluate given lua code (demands current user to have 'execute' rights for 'universe' in Tarantool)
     *
     * @see https://github.com/tarantool/tarantool-php#tarantoolevaluate
     * @param string $expression
     * @param mixed|null $args [optional]
     * @return mixed
     * @throws Exception
     */
    public function evaluate(string $expression, $args = null)
    {
    }

    /**
     * Delete record with given key.
     *
     * @see https://github.com/tarantool/tarantool-php#tarantooldelete
     * @param string|int $space
     * @param string|int|array $key
     * @param string|int $index [optional, default=0]
     * @return array
     * @throws Exception
     */
    public function delete($space, $key, $index = 0): array
    {
    }

    /**
     * Update record with given key (update in Tarantool is apply multiple given operations to tuple)
     *
     * @see https://github.com/tarantool/tarantool-php#tarantoolupdate
     * @param string|int $space
     * @param string|int|array $key
     * @param array[] $ops
     * @param string|int $index [optional, default=0]
     * @return array
     * @throws Exception
     */
    public function update($space, $key, array $ops, $index = 0): array
    {
    }

    /**
     * Update or Insert command (If tuple with PK == PK('tuple') exists, then it'll update this tuple with 'ops',
     * otherwise 'tuple' will be inserted)
     *
     * @see https://github.com/tarantool/tarantool-php#tarantoolupsert
     * @param string|int $space
     * @param string|int|array $key
     * @param array[] $ops
     * @param string|int $index [optional, default=0]
     * @return void|array
     * @throws Exception
     */
    public function upsert($space, $key, array $ops, $index = 0)
    {
    }
}

